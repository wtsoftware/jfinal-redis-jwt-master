Table: t_user
------------+--------------+------+-----+---------+---------
 Field      | Type         | Null | Key | Default | Remarks 
------------+--------------+------+-----+---------+---------
 userId     | BIGINT(19)   | NO   | PRI |         | 用户id    
 username   | VARCHAR(30)  | NO   |     |         | 用户名     
 password   | VARCHAR(100) | NO   |     |         | 密码      
 nickName   | VARCHAR(30)  | NO   |     |         | 昵称      
 avatar     | VARCHAR(300) | NO   |     |         | 头像      
 gender     | VARCHAR(10)  | NO   |     |         | 性别      
 createTime | DATETIME(19) | NO   |     |         | 创建时间    
------------+--------------+------+-----+---------+---------

