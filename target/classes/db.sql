/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 5.0.96-community-nt : Database - jfinal_jwt_demo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jfinal_jwt_demo` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `jfinal_jwt_demo`;

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `userId` bigint(20) NOT NULL auto_increment COMMENT '用户id',
  `username` varchar(30) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `nickName` varchar(30) NOT NULL COMMENT '昵称',
  `avatar` varchar(300) NOT NULL COMMENT '头像',
  `gender` varchar(10) NOT NULL COMMENT '性别',
  `accountStatus` int(1) NOT NULL COMMENT '账号状态[0:禁用,1:正常,2:失效]',
  `createTime` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`userId`,`username`,`password`,`nickName`,`avatar`,`gender`,`accountStatus`,`createTime`) values 
(1,'Alston','D68D99AFEEA9EF0540C406E9F0B497E9','墨雨轩','','男',1,'2018-07-02 15:09:47'),
(2,'Andrew','D68D99AFEEA9EF0540C406E9F0B497E9','蓝月之谷','','男',1,'2018-07-02 15:10:26');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
