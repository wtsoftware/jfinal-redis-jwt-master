package com.alston.rest.controller.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.alston.rest.common.Require;
import com.alston.rest.common.bean.BaseResponse;
import com.alston.rest.common.bean.Code;
import com.alston.rest.common.utils.StringUtils;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Record;


/**
 * 基本的Controller
 * 基于jfinal controller做一些封装
 * @author AndrewTseng
 */
public class BaseController extends Controller{
	private static Log log = Log.getLog(BaseController.class);
	 /**
     * 获取当前用户对象
     * @return
     */
    protected Record getUser() {
    	Record member = getAttr("member");
//        if (student == null) {
//            String token = getPara("token");
//            return Team.dao.findValidByToken(token);
//        }
        return getAttr("member");
    }
	
	
	
    /**
     * 响应接口不存在*
     */
    public void render404() {
        renderJson(new BaseResponse(Code.NOT_FOUND));
        
    }

    /**
     * 响应请求参数有误*
     * @param message 错误信息
     */
    public void renderArgumentError(String message) {
        renderJson(new BaseResponse(Code.ARGUMENT_ERROR, message));

    }

    /**
     * 响应操作成功*
     * @param message 响应信息
     */
    public void renderSuccess(String message) {
        renderJson(new BaseResponse().setMsg(message));
        
    }

    /**
     * 响应操作失败*
     * @param message 响应信息
     */
    public void renderFailed(String message) {
        renderJson(new BaseResponse(0, message));
        
    }
    
    /**
     * 判断请求类型是否相同*
     * @param name
     * @return
     */
    protected boolean methodType(String name) {
        return getRequest().getMethod().equalsIgnoreCase(name);
    }
    
    
    /**
     * 判断参数值是否为空
     * @param rules
     * @return
     */
    public boolean notNull(Require rules) {

        if (rules == null || rules.getLength() < 1) {
            return true;
        }

        for (int i = 0, total = rules.getLength(); i < total; i++) {
            Object key = rules.get(i);
            String message = rules.getMessage(i);
            BaseResponse response = new BaseResponse(Code.ARGUMENT_ERROR);
            
            if (key == null) {
                renderJson(response.setMsg(message));
                return false;
            }

            if (key instanceof String && StringUtils.isEmpty((String) key)) {
                renderJson(response.setMsg(message));
                return false;
            }

            if (key instanceof Array) {
                Object[] arr = (Object[]) key;

                if (arr.length < 1) {
                    renderJson(response.setMsg(message));
                    return false;
                }
            }
        }

        return true;
    }
    
    
    
  
    /*
	 * 
	 *打印结果方便调试
	 */
	public void renderJson(Object object) {
		HttpServletResponse res = getResponse();
		//res.setHeader("Access-Control-Allow-Credentials", "true");
		//res.addHeader("Access-Control-Allow-Origin", "*");
		//res.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:8080"); //本地环境
		//res.setHeader("Access-Control-Allow-Origin", "http://192.168.1.252:8080");//测试环境
		//res.setHeader("Access-Control-Allow-Origin", "cloud.zyveke.com/zywc");//生产环境
		//res.setHeader("Access-Control-Allow-Origin", "http://112.74.86.119:8050");//生产环境
		String originHeader = getRequest().getHeader("Origin");
        String[] IPs = {"http://127.0.0.1:8080","http://192.168.1.252:8080","http://112.74.86.119:8050"};
        if (Arrays.asList(IPs).contains(originHeader)){
        	res.setHeader("Access-Control-Allow-Origin", originHeader);
        }
		
		res.setHeader("Access-Control-Allow-Credentials", "true");
		res.setHeader("Access-Control-Allow-Headers", "Content-Type,Authorization");
		res.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
		res.setHeader("Content-Type", "application/json;charset=utf-8");
		System.out.println(JsonKit.toJson(object));

		super.renderJson(object);
	}

	
    
	/**
     * 取Request中的数据对象
     * @param valueType
     * @return
     * @throws Exception
     */
    public JSONObject getParaToJsonObject()  {
    	JSONObject jo = new JSONObject();
        StringBuilder json = new StringBuilder();
        BufferedReader reader;
		try {
			reader = getRequest().getReader();
			String line = null;
	        while((line = reader.readLine()) != null){
	            json.append(line);
	        }
	        reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("Can not parse the parameter \"" + json.toString() + "\" to JSONObject.");
			//e.printStackTrace();
		}
       
        jo = JSONObject.parseObject(json.toString());
        
        log.info("学员:【无登录信息】"
				   + "请求方法:【"+getRequest().getRequestURI()+"】"
				   + "请求参数:【"+json+"】"
				   + "返回数据:"+"Object");
        
        
        return jo;
    }
}
