package com.alston.rest.interceptor;

import com.alston.rest.common.bean.BaseResponse;
import com.alston.rest.common.bean.Code;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.log.Log;

/**
 * 捕获所有api action异常
 * @author cnmobi_db
 */
public class ErrorInterceptor implements Interceptor {
	private static Log log = Log.getLog(ErrorInterceptor.class);

    public void intercept(Invocation inv) {
		try {
			inv.invoke();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            log.error(e.getMessage(), e);
            inv.getController().renderJson(new BaseResponse(Code.ERROR, "server error："+e.getMessage()));
        }
	}
}
