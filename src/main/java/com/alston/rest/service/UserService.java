package com.alston.rest.service;

import com.alston.rest.model.User;

public interface UserService {
	public User findUserByUsername(String username);
	
}
