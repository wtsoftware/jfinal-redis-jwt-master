package com.alston.rest.service.impl;

import com.alston.rest.model.User;
import com.alston.rest.service.UserService;

public class UserServiceImpl  implements UserService{
	public User findUserByUsername(String username){
		return User.dao.findFirst("select * from t_user where username=?",username);
	}

}
