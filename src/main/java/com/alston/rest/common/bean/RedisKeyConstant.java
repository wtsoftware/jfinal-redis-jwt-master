package com.alston.rest.common.bean;

/** 
* @ClassName: RedisKeyConstant 
* @Description: Redis键值常量
* @author alston 
* @date 2018年5月23日 下午9:11:24 
*  
*/
public class RedisKeyConstant {
	
	public final static String FIND_TOKEN_BY_USER_ID = "jwt:token:user.userid:";
	
	public final static String FIND_USER_BY_TOKEN = "jwt:user:token:";
	
}
